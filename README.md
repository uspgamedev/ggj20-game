# ggj20-game

2D platformer where you are a little robot. You fell in a hole and you got destroyed. You start with a torso and an arm. You need to find resources to build your other limbs. It is a metroidvania where you will start being able to get to new places as soon as you are getting more limbs.

The game is physics-based so that it is a lot of fun to play.

The parts that the robot have are:

1. Head: It has 1 connection slot. It can slightly impulse forward or backward.
2. Torso: It has 2 connection slots. Its sole purpose is to add connection slots to connect other pieces.
3. Arm: Can be controlled analogically and has a triggerable grip that can open/close, and grab at the floor/wall.
4. Hook: Arm that can shoot a grappling hook that sticks in the wall/ceiling.
5. Spring: Piece that can propell the robot upward by pushing strongly the floor
6. Jetpack: Propels the robot upward
7. Rocket launcher: Shoots a missile that can brake specific clearly differntiated parts of the level.
 
Note: Some parts may not be included in the game.

# Install

Use Godot 3.2-stable.
