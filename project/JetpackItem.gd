tool
extends Spatial

func _on_Area_body_entered(body):
	if body.is_in_group("robot"):
		body.get_parent().has_jetpack = true
		queue_free()
