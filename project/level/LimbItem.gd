tool
extends Spatial

#signal limb_collected

func _ready():
	$"Limb/robo-foot/Cylinder001".mesh.surface_set_material(1, $"Limb/robo-foot/Cylinder001".mesh.surface_get_material(1).duplicate())

func _process(delta):
	$Limb.rotation_degrees.y += 100 * delta

func _on_Area_body_entered(body):
	if body.is_in_group("robot"):
#		emit_signal("limb_collected")
		body.get_parent().freeze($OmniLight)
		yield(increase_glow(), "completed")
		GlobalFade.fade_out()
		yield(GlobalFade.tween, "tween_completed")
		get_tree().change_scene("res://UI/MainMenu.tscn")
		GlobalFade.fade_in()

func increase_glow():
	var tween = $Tween
	tween.interpolate_property($OmniLight, "omni_range", $OmniLight.omni_range, 20, 2.0, Tween.TRANS_LINEAR, Tween.EASE_IN)
	tween.interpolate_property($OmniLight, "light_energy", $OmniLight.light_energy, 5, 2.0, Tween.TRANS_LINEAR, Tween.EASE_IN)
	tween.interpolate_property($OmniLight, "translation:z", $OmniLight.translation.z, $OmniLight.translation.z + 10.0, 2.0, Tween.TRANS_LINEAR, Tween.EASE_IN)
	tween.start()
	yield(tween, "tween_completed")
