extends Camera

func _process(_delta):
	var z = global_transform.origin.z
	global_transform.origin = $"../Robot/Robot_Core".global_transform.origin
	global_transform.origin.z = z
