extends Spatial


func _unhandled_input(event):
	if event is InputEventKey:
		if event.scancode == KEY_R:
			GlobalFade.fade_out()
			
			yield(GlobalFade.tween, "tween_completed")
			
			get_tree().reload_current_scene()
			GlobalFade.fade_in()
		elif event.scancode == KEY_P:
			$PauseScreen.pause()
