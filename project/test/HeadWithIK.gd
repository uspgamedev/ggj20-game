extends RigidBody

onready var tip_bone_pivot = get_node("../TipBonePivot")

const VEL = 200
const LIMB_LENGTH = 6.5
const EASY_FLY_IMPULSE = false

var last_mouse_pos : Vector2

func _ready():
	friction = 1.0
	$CollisionShape.make_convex_from_brothers()
	tip_bone_pivot.global_transform.origin = $Limb.global_transform.origin + Vector3(7, 0, 0)
	$Limb/Armature/Skeleton/SkeletonIK.target_node = "../../../../../TipBonePivot"
	$Limb/Armature/Skeleton/SkeletonIK.start()

func _process(delta):
	if not tip_bone_pivot.is_colliding:
		if last_mouse_pos and not EASY_FLY_IMPULSE:
			linear_velocity = Vector3(clamp(linear_velocity.x, -50, 50), clamp(linear_velocity.x, -40, 40), 0)
			linear_velocity *= 0.2
		
		last_mouse_pos =  Vector2.ZERO
		return
		
#	move_head_with_WASD(delta)
	move_head_with_mouse(delta)
	
	$Limb.global_transform.origin = $RootBonePivot.global_transform.origin
	var pivot_pos = tip_bone_pivot.global_transform.origin
	if pivot_pos.distance_to(global_transform.origin) > LIMB_LENGTH:
		global_transform.origin = global_transform.origin + \
		(pivot_pos - global_transform.origin).normalized() * \
		(pivot_pos.distance_to(global_transform.origin) - LIMB_LENGTH)
		
	last_mouse_pos = get_viewport().get_mouse_position()

func move_head_with_mouse(delta):
	if not last_mouse_pos:
		return
	
	var diff = get_viewport().get_mouse_position() - last_mouse_pos
	linear_velocity = VEL * Vector3(diff.x, -diff.y, 0) * delta
		
	
func move_head_with_WASD(delta):
	var dir = Vector3.ZERO
	if Input.is_key_pressed(KEY_W):
		dir += Vector3(0, 1, 0)
	if Input.is_key_pressed(KEY_S):
		dir += Vector3(0, -1, 0)
	if Input.is_key_pressed(KEY_A):
		dir += Vector3(-1, 0, 0)
	if Input.is_key_pressed(KEY_D):
		dir += Vector3(1, 0, 0)
	linear_velocity = VEL * dir * delta
