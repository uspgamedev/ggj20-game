extends KinematicBody

const LIMB_LENGTH = 6.5

onready var head = get_node("../Head")
onready var camera = get_node("../../Camera")
var last_mouse_pos : Vector2
var is_colliding := false

func _ready():
	move_lock_z = true

func _process(delta):
	if last_mouse_pos and Input.is_key_pressed(KEY_SPACE):
		var rel_vec = (camera.project_position(get_viewport().get_mouse_position(), \
		camera.translation.z) * Vector3(1, 1, 0) - camera.project_position(last_mouse_pos, camera.translation.z) * Vector3(1, 1, 0))
		var collision = move_and_collide(rel_vec)
		var mesh : MeshInstance = get_node("../Head/Limb/Armature/Skeleton/Arm_Root001")
		if collision:
			is_colliding = true
			head.gravity_scale = 0.0
			mesh.mesh.surface_get_material(2).albedo_color = Color.white
		elif rel_vec != Vector3.ZERO:
			is_colliding = false
			head.gravity_scale = 2.0
			mesh.mesh.surface_get_material(2).albedo_color = Color.black
			
		var head_pos = head.global_transform.origin
		if head_pos.distance_to(global_transform.origin) > LIMB_LENGTH:
			global_transform.origin = global_transform.origin + (head_pos - global_transform.origin).normalized() * (head_pos.distance_to(global_transform.origin) - LIMB_LENGTH)
			
	last_mouse_pos = get_viewport().get_mouse_position()
