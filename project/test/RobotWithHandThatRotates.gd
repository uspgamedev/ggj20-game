extends Spatial

const LIMB_LENGTH := 6.3
const VEL := 200

var is_colliding := false
var last_mouse_pos : Vector2
var old_mouse : Vector2
var old_hand  : Vector3

func _ready():
#	$Robot_Core.mode = RigidBody.MODE_STATIC
	$Robot_Left_Hand.gravity_scale = 0

func project_from_cam(vec, cam):
	var world_vec = cam.project_position(vec, cam.translation.z)
	return world_vec * Vector3(1, 1, 0)

func move_with_mouse(delta):
	if is_colliding:
		return
	
	var new_mouse = get_viewport().get_mouse_position()
	var camera    = get_viewport().get_camera()
	
	var new_world = project_from_cam(new_mouse, camera)
	var ball_pos  = $Robot_Core.global_transform.origin
#	new_world.x = max ( new_world.x, ball_pos.x )
	
	var targ_pos  = $Robot_Left_Hand.global_transform.origin
	var mouse_mov = new_world - targ_pos * Vector3(1,1,0)
	
	var max_vel = 500 * delta
	var new_vel = max_vel * mouse_mov.normalized() * \
		min(mouse_mov.length(), 1)
	
	if (new_vel.length() > max_vel):
		new_vel = new_vel.normalized() * max_vel
	
	var distance := 1.0
	if new_vel.dot(targ_pos - ball_pos) > 0:
		distance = ease(inverse_lerp(LIMB_LENGTH, 0, ball_pos.distance_to(targ_pos)), 0.1)
	
	$Robot_Left_Hand.linear_velocity = new_vel * distance
	# maybe consider adding negative vel to other part for newton

func move_head_if_allowed(delta):
	var head = $Robot_Core
#	$Robot_Left_Hand.mode = RigidBody.MODE_STATIC if Input.is_key_pressed(KEY_SPACE) else RigidBody.MODE_RIGID
	if Input.is_key_pressed(KEY_SPACE):
		var mesh : MeshInstance = get_node("../Cylinder002")
		var area = $Robot_Left_Hand/Area
		for body in area.get_overlapping_bodies():
			if body.is_in_group("floor"):
				is_colliding = true
				head.gravity_scale = 0.0
				mesh.mesh.surface_get_material(2).albedo_color = Color.white
				break
				
		if not is_colliding:
			head.gravity_scale = 1.0
			mesh.mesh.surface_get_material(2).albedo_color = Color.black
			
		set_limit(head, self)
	else:
		is_colliding = false
	
	last_mouse_pos = get_viewport().get_mouse_position()

func set_limit(from, to):
	var from_pos = from.global_transform.origin
	var to_pos = to.global_transform.origin
	if from_pos.distance_to(to_pos) > LIMB_LENGTH:
		to.global_transform.origin = to_pos + (from_pos - to_pos).normalized() * (from_pos.distance_to(to_pos) - LIMB_LENGTH)

func move_head_with_WASD(delta):
	var dir = Vector3.ZERO
	if Input.is_key_pressed(KEY_W):
		dir += Vector3.UP
	if Input.is_key_pressed(KEY_S):
		dir += Vector3.DOWN
	if Input.is_key_pressed(KEY_A):
		dir += Vector3.LEFT
	if Input.is_key_pressed(KEY_D):
		dir += Vector3.RIGHT
	$Robot_Core.linear_velocity = VEL * dir * delta

func _process(delta):
	move_with_mouse(delta)
	move_head_if_allowed(delta)
	if is_colliding:
		move_head_with_WASD(delta)
	old_hand  = $Robot_Left_Hand.global_transform.origin
	old_mouse = get_viewport().get_mouse_position()
