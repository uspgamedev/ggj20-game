extends Spatial

func _ready():
	$Armature/Skeleton/SkeletonIK.target_node = "../../../../../KinematicBody"
	$Armature/Skeleton/SkeletonIK.start()

func _process(delta):
	$GripIndicator.global_transform.origin = get_node('../../KinematicBody').global_transform.origin
	$GripIndicator.visible = get_node('../../KinematicBody').is_colliding
	
