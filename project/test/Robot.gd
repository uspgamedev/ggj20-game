extends Spatial

var old_mouse : Vector2
var old_hand  : Vector3

func _ready():
	$Robot_Core.mode = RigidBody.MODE_STATIC
	$Robot_Left_Hand.gravity_scale = 0

func project_from_cam(vec, cam):
	var world_vec = cam.project_position(vec, cam.translation.z)
	return world_vec * Vector3(1, 1, 0)

func move_with_mouse(delta):
	var new_mouse = get_viewport().get_mouse_position()
	var camera    = get_node("../Camera")
	
	var new_world = project_from_cam(new_mouse, camera)
	var ball_pos  = $Robot_Core.global_transform.origin
	new_world.x = max ( new_world.x, ball_pos.x )
	
	var targ_pos  = $Robot_Left_Hand.global_transform.origin
	var mouse_mov = new_world - targ_pos * Vector3(1,1,0)
	
	var max_vel = 500 * delta
	var new_vel = max_vel * mouse_mov.normalized() * \
		min(mouse_mov.length(), 1)
	
	if (new_vel.length() > max_vel):
		new_vel = new_vel.normalized() * max_vel
	
	$Robot_Left_Hand.linear_velocity = new_vel

func orient_hand(delta):
	pass
	

func _process(delta):
	move_with_mouse(delta)
	old_hand  = $Robot_Left_Hand.global_transform.origin
	old_mouse = get_viewport().get_mouse_position()
