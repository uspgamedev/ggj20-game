extends RigidBody

func _process(delta):
	if Input.is_key_pressed(KEY_D):
		add_torque(Vector3.FORWARD * 10)
	if Input.is_key_pressed(KEY_A):
		add_torque(Vector3.BACK * 10)

func _ready():
	friction = 1.0
