extends Sprite3D

export(String, MULTILINE) var text = "(Change me)"

func _ready():
	texture = $Viewport.get_texture()
	
	set_text(text)

func set_text(string: String):
	 $Viewport/Label.text = string
