extends CanvasLayer

onready var background = $Background
onready var buttons = $Background/Buttons
onready var resume_button = $Background/Buttons/Resume
onready var tween = $Tween
onready var options_menu = $Background/OptionsMenu
onready var options_button = $Background/Buttons/Options
onready var confirmation = $Background/Confirmation
onready var no_button = $Background/Confirmation/ConfirmationButtons/No
onready var menu_button = $Background/Buttons/MainMenu
onready var quit_button = $Background/Buttons/Quit

const BG_COLOR = Color("15151b")
const DISABLED_COLOR = Color("808080")
const TWN_DUR = 0.6
const OPT_POS = Vector2(550, 300)
const STYLE_SELECTED_DISABLED = preload("res://UI/selected_disabled.tres")
const STYLE_REGULAR_DISABLED = preload("res://UI/regular_disabled.tres")

signal confirmation_signal

func _ready():
	background.hide()
	confirmation.hide()
	options_menu.connect("back_pressed", self, "maximize_menu_left")


func disable_buttons():
	for button in buttons.get_children():
		button.disabled = true
		button.focus_mode = Button.FOCUS_NONE

func enable_buttons():
	for button in buttons.get_children():
		button.disabled = false
		button.focus_mode = Button.FOCUS_ALL

func minimize_menu_left():
	disable_buttons()
	options_button.set("custom_colors/font_color_disabled", BG_COLOR)
	options_button.set("custom_styles/disabled", STYLE_SELECTED_DISABLED)
	
	tween.interpolate_property(buttons, "rect_scale", null, Vector2(0.7, 0.7),
			TWN_DUR, Tween.TRANS_CUBIC, Tween.EASE_IN_OUT)
	tween.interpolate_property(buttons, "rect_position", null, Vector2(150, 300),
			TWN_DUR, Tween.TRANS_CUBIC, Tween.EASE_IN_OUT)
	tween.interpolate_property(options_menu, "rect_position", null, OPT_POS,
			TWN_DUR, Tween.TRANS_CUBIC, Tween.EASE_IN_OUT)
	tween.start()
	
	yield(tween, "tween_completed")
	
	options_menu.enable_buttons()
	options_menu.music_slider.grab_focus()

func maximize_menu_left():
	options_button.set("custom_colors/font_color_disabled", DISABLED_COLOR)
	options_button.set("custom_styles/disabled", STYLE_REGULAR_DISABLED)
	options_menu.disable_buttons()
	
	tween.interpolate_property(buttons, "rect_scale", null, Vector2(1, 1),
			TWN_DUR, Tween.TRANS_CUBIC, Tween.EASE_IN_OUT)
	tween.interpolate_property(buttons, "rect_position", null, Vector2(437, 300),
			TWN_DUR, Tween.TRANS_CUBIC, Tween.EASE_IN_OUT)
	tween.interpolate_property(options_menu, "rect_position", null, Vector2(550, 600),
			TWN_DUR, Tween.TRANS_CUBIC, Tween.EASE_IN_OUT)
	tween.start()
	
	yield(tween, "tween_completed")
	
	enable_buttons()
	options_button.grab_focus()


func pause():
	get_tree().paused = true
	background.show()
	resume_button.grab_focus()


func _on_Resume_pressed():
	background.hide()
	get_tree().paused = false


func _on_Options_pressed():
	minimize_menu_left()


func _on_MainMenu_pressed():
	buttons.hide()
	confirmation.show()
	no_button.grab_focus()
	var conf = yield(self, "confirmation_signal")
	if conf == 1:
		GlobalFade.fade_out()
		yield(GlobalFade.tween, "tween_completed")
		get_tree().change_scene("res://UI/MainMenu.tscn")
		get_tree().paused = false
		GlobalFade.fade_in()
	else:
		buttons.show()
		confirmation.hide()
		menu_button.grab_focus()

func _on_Quit_pressed():
	buttons.hide()
	confirmation.show()
	no_button.grab_focus()
	var conf = yield(self, "confirmation_signal")
	if conf == 1:
		get_tree().quit()
	else:
		buttons.show()
		confirmation.hide()
		quit_button.grab_focus()


func _on_Yes_pressed():
	emit_signal("confirmation_signal", 1)


func _on_No_pressed():
	emit_signal("confirmation_signal", 0)
