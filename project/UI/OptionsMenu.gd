extends VBoxContainer

onready var music_slider = $Music
onready var sfx_slider = $SFX
onready var back_button = $Back
signal back_pressed

func _ready():
	change_label_value($Music/Value, Global.music_volume)
	$Music.value = Global.music_volume
	change_label_value($SFX/Value, Global.sfx_volume)
	$SFX.value = Global.sfx_volume
	disable_buttons()


func change_label_value(label : Label, value):
	label.text = str(value)


func _on_Music_value_changed(value):
	change_label_value($Music/Value, value)
	Global.music_volume = value
	AudioServer.set_bus_volume_db(Global.BGM_INDEX, linear2db(value/100))


func _on_SFX_value_changed(value):
	change_label_value($SFX/Value, value)
	Global.sfx_volume = value
	AudioServer.set_bus_volume_db(Global.SFX_INDEX, linear2db(value/100))

func disable_buttons():
	$Music.editable = false
	$SFX.editable = false
	$Back.disabled = true

func enable_buttons():
	$Music.editable = true
	$SFX.editable = true
	$Back.disabled = false


func _on_Back_pressed():
	emit_signal("back_pressed")
