extends Control

onready var buttons = $TitleScreen/Buttons
onready var start_button = $TitleScreen/Buttons/Start
onready var tween = $Tween
onready var options_menu = $OptionsMenu
onready var options_button = $TitleScreen/Buttons/Options
onready var credits_text = $CreditsText
onready var credits_button = $TitleScreen/Buttons/Credits
onready var title_screen = $TitleScreen

const BG_COLOR = Color("15151b")
const DISABLED_COLOR = Color("808080")
const TWN_DUR = 0.6
const OPT_POS = Vector2(550, 300)
const CRED_POS = Vector2(120, 180)
const BUTTONS_POS = Vector2(437, 298.5)
const STYLE_SELECTED_DISABLED = preload("res://UI/selected_disabled.tres")
const STYLE_REGULAR_DISABLED = preload("res://UI/regular_disabled.tres")
const STYLE_START_DISABLED = preload("res://UI/start_disabled.tres")

func _ready():
	start_button.grab_focus()
	options_menu.connect("back_pressed", self, "maximize_menu_left")
	credits_text.back_button.connect("pressed", self, "maximize_menu_right")


func disable_buttons():
	for button in buttons.get_children():
		button.disabled = true
		button.focus_mode = FOCUS_NONE

func enable_buttons():
	for button in buttons.get_children():
		button.disabled = false
		button.focus_mode = FOCUS_ALL

func minimize_menu_left():
	$"Select".play()
	options_menu.show()
	disable_buttons()
	options_button.set("custom_colors/font_color_disabled", BG_COLOR)
	options_button.set("custom_styles/disabled", STYLE_SELECTED_DISABLED)
	
	tween.interpolate_property(buttons, "rect_scale", null, Vector2(0.7, 0.7),
			TWN_DUR, Tween.TRANS_CUBIC, Tween.EASE_IN_OUT)
	tween.interpolate_property(buttons, "rect_position", null, Vector2(150, 300),
			TWN_DUR, Tween.TRANS_CUBIC, Tween.EASE_IN_OUT)
	tween.interpolate_property(options_menu, "rect_position", null, OPT_POS,
			TWN_DUR, Tween.TRANS_CUBIC, Tween.EASE_IN_OUT)
	tween.start()
	
	yield(tween, "tween_completed")
	
	options_menu.enable_buttons()
	options_menu.music_slider.grab_focus()

func minimize_menu_right():
	$"Select".play()
	credits_text.show()
	disable_buttons()
	credits_button.set("custom_colors/font_color_disabled", BG_COLOR)
	credits_button.set("custom_styles/disabled", STYLE_SELECTED_DISABLED)
	
	tween.interpolate_property(buttons, "rect_scale", null, Vector2(0.7, 0.7),
			TWN_DUR, Tween.TRANS_CUBIC, Tween.EASE_IN_OUT)
	tween.interpolate_property(buttons, "rect_position", null, Vector2(724, 300),
			TWN_DUR, Tween.TRANS_CUBIC, Tween.EASE_IN_OUT)
	tween.interpolate_property(credits_text, "rect_position", null, CRED_POS,
			TWN_DUR, Tween.TRANS_CUBIC, Tween.EASE_IN_OUT)
	tween.start()
	
	yield(tween, "tween_completed")
	
	credits_text.back_button.disabled = false
	credits_text.back_button.grab_focus()
	

func maximize_menu_left():
	$"Select".play()
	options_button.set("custom_colors/font_color_disabled", DISABLED_COLOR)
	options_button.set("custom_styles/disabled", STYLE_REGULAR_DISABLED)
	options_menu.disable_buttons()
	
	tween.interpolate_property(buttons, "rect_scale", null, Vector2(1, 1),
			TWN_DUR, Tween.TRANS_CUBIC, Tween.EASE_IN_OUT)
	tween.interpolate_property(buttons, "rect_position", null, BUTTONS_POS,
			TWN_DUR, Tween.TRANS_CUBIC, Tween.EASE_IN_OUT)
	tween.interpolate_property(options_menu, "rect_position", null, Vector2(550, 600),
			TWN_DUR, Tween.TRANS_CUBIC, Tween.EASE_IN_OUT)
	tween.start()
	
	yield(tween, "tween_completed")
	
	enable_buttons()
	options_button.grab_focus()
	options_menu.hide()

func maximize_menu_right():
	$"Select".play()
	credits_button.set("custom_colors/font_color_disabled", DISABLED_COLOR)
	credits_button.set("custom_styles/disabled", STYLE_REGULAR_DISABLED)
	credits_text.back_button.disabled = true
	
	tween.interpolate_property(buttons, "rect_scale", null, Vector2(1, 1),
			TWN_DUR, Tween.TRANS_CUBIC, Tween.EASE_IN_OUT)
	tween.interpolate_property(buttons, "rect_position", null, BUTTONS_POS,
			TWN_DUR, Tween.TRANS_CUBIC, Tween.EASE_IN_OUT)
	tween.interpolate_property(credits_text, "rect_position", null, Vector2(120, 600),
			TWN_DUR, Tween.TRANS_CUBIC, Tween.EASE_IN_OUT)
	tween.start()
	
	yield(tween, "tween_completed")
	
	enable_buttons()
	credits_button.grab_focus()
	credits_text.hide()

func slide_menu_up():
	start_button.set("custom_colors/font_color_disabled", BG_COLOR)
	start_button.set("custom_styles/disabled", STYLE_START_DISABLED)
	disable_buttons()
	
	tween.interpolate_property(title_screen, "rect_position", null, Vector2(0, -600),
			TWN_DUR, Tween.TRANS_CUBIC, Tween.EASE_IN_OUT)
	tween.start()
	yield(tween, "tween_completed")
	GlobalFade.fade_out()
	yield(GlobalFade.tween, "tween_completed")
	get_tree().change_scene("res://level/test/TestLevel.tscn")
	GlobalFade.fade_in()


func _on_Start_pressed():
	$"Start".play()
	slide_menu_up()


func _on_Options_pressed():
	minimize_menu_left()


func _on_Credits_pressed():
	disable_buttons()
	minimize_menu_right()


func _on_Quit_pressed():
	get_tree().quit()


func _on_Button_mouse_entered():
	$Hover.play()
