extends CanvasLayer

onready var tween = $Tween

const DURATION = 0.6

func fade_out():
	tween.interpolate_property($Fade, "color:a", 0, 1, DURATION, Tween.TRANS_CUBIC, Tween.EASE_IN_OUT)
	tween.start()

func fade_in():
	tween.interpolate_property($Fade, "color:a", 1, 0, DURATION, Tween.TRANS_CUBIC, Tween.EASE_IN_OUT)
	tween.start()
