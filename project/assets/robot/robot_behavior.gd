extends Spatial
class_name Robot_Behavior

enum Mode {
	LeftGrab,
	RightGrab
}


const key_to_dir := {
	KEY_W : Vector3.UP,
	KEY_S : Vector3.DOWN,
	KEY_A : Vector3.LEFT,
	KEY_D : Vector3.RIGHT,
}

var has_jetpack := false
var modes : Array
var search : Array
var camera_offset : Vector3
var jetpack_cooldown := 0.0

func _ready():
#	$Robot_Core.mode = RigidBody.MODE_STATIC
	modes  = Array()
	search = Array()
	if ($Robot_Left_Hand):
	#	$Robot_Left_Hand.gravity_scale = 0
		search.append(Mode.LeftGrab)
	camera_offset = $Camera.translation

func _project_from_camera(vec2, cam):
	var res = cam.project_position(vec2, cam.translation.z)
	return res * Vector3(1, 1, 0)

func _get_head():
	return $Robot_Core

func _get_lhand():
	if has_node("Robot_Left_Hand"):
		return $Robot_Left_Hand
	else:
		return null

func _get_camera():
	var camera = get_viewport().get_camera()
	return camera

func _get_cursor():
	return get_viewport().get_mouse_position()

func _try_enable_grab(hand, state):
	if modes.has(state):
		return false
	var area = hand.get_node("Area")
	var overlapping = area.get_overlapping_bodies()
	for body in overlapping:
		if body.is_in_group("floor"):
			modes.append(state)
			search.erase(state)
			hand.mode = RigidBody.MODE_STATIC
			_get_head().gravity_scale -= 0.5
			return true
	return false

func _try_disable_grab(hand, state):
	if ! modes.has(state):
		return false
	_get_head().gravity_scale += 0.5
	hand.mode = RigidBody.MODE_RIGID
	search.append(state)
	modes.erase(state)
	return true

func move_hands(delta):
	var max_vel = delta * 50
	
	var selected_point = _project_from_camera(
		_get_cursor(),
		_get_camera()
	)
	
	var robot_head_pos  = _get_head().global_transform.origin
	var robot_lhand_pos = _get_lhand().global_transform.origin
	var target_offset   = selected_point - robot_lhand_pos * \
		Vector3(1,1,0)
	var hand_velocity  = max_vel * target_offset.normalized()\
		* min ( target_offset.length(), 1 )
	
	# clamp vector
	if  hand_velocity.length() > max_vel:
		hand_velocity = hand_velocity.normalized() * max_vel
	
	_get_lhand().apply_central_impulse(hand_velocity)
	_get_head() .apply_central_impulse(hand_velocity)

func force_inbound(anchor, other, lim):
	var p1 = anchor.global_transform.origin
	var p2 =  other.global_transform.origin
	
	var dist = p1.distance_to(p2)
	if (dist > lim):
		other.global_transform.origin += \
		(p1 - p2).normalized() * (dist - lim)

func apply_force_to_head(force):
	_get_head().apply_central_impulse(force)
	#var limb_size = 7.0
	#if modes.has(Mode.LeftGrab):
	#	force_inbound(_get_lhand(), _get_head(), limb_size)
#	if modes.has(Mode.RightGrab):
#		force_inbound(_get_rhand(), _get_head(), limb_size)

func roll_head(delta):
	var max_vel = 20
	var dir = Vector3.ZERO
	if Input.is_key_pressed(KEY_A):
		dir += Vector3(-1, 0, 0)
	if Input.is_key_pressed(KEY_D):
		dir += Vector3(1, 0, 0)

	apply_force_to_head(max_vel * dir)

func lift_head(delta):
	var max_vel = 5
	var dir = Vector3.ZERO
#	if Input.is_key_pressed(KEY_W):
#		dir += Vector3(0, 1, 0)
#	if Input.is_key_pressed(KEY_S):
#		dir += Vector3(0, -1, 0)
	for key in key_to_dir:
		if Input.is_key_pressed(key):
			dir += key_to_dir[key]
	apply_force_to_head(max_vel * dir)

func update_state():
	if Input.is_action_just_pressed ("left_attatch"):
		_try_enable_grab( _get_lhand(), Mode.LeftGrab)
	if Input.is_action_just_released("left_attatch"):
		_try_disable_grab(_get_lhand(), Mode.LeftGrab)

func update_camera():
	$Camera.global_transform.origin = \
		$Robot_Core.global_transform.origin
	$Camera.global_transform.origin.z = 20
	$Camera.global_transform.basis = get_parent().global_transform.basis
	# = \
	#lerp($Camera.global_transform.origin, _get_head().global_transform.origin + camera_offset, 0.1)

func freeze(light):
	set_process(false)
	_get_head().linear_velocity = Vector3.ZERO
	_get_head().gravity_scale = 0.0
	var tween = Tween.new()
	add_child(tween)
	var target = light.global_transform.origin
	target.z = 10.0
	tween.interpolate_property($Camera, "global_transform:origin", $Camera.global_transform.origin, target, 1.0, Tween.TRANS_CUBIC, Tween.EASE_IN_OUT)
	tween.start()

func update_jetpack(delta):
	$JetpackBar.global_transform.origin = \
		$Robot_Core.global_transform.origin
	$JetpackBar.global_transform.origin.y += 1.5
	$JetpackBar.global_transform.basis = get_parent().global_transform.basis
	$JetpackBar.rotation_degrees.x = 90.0
	
	$JetpackParticles.global_transform.origin = \
		$Robot_Core.global_transform.origin
	$JetpackParticles.rotation_degrees.x = 90.0
	$JetpackParticles.rotation_degrees.y = -180.0
	
	if not has_jetpack:
		return
	
	var bar = $JetpackBar.mesh.mid_height
	var jet_particles := false
	if Input.is_key_pressed(KEY_SHIFT) and bar > 0.0:
		$JetpackBar.mesh.mid_height -= delta * 2
		jetpack_cooldown = clamp(jetpack_cooldown - delta, 0, 1.0)
		_get_head().apply_central_impulse(Vector3(0, 5, 0))
		jet_particles = true
	elif not Input.is_key_pressed(KEY_SHIFT):
		jetpack_cooldown = min(jetpack_cooldown + delta, 3.0)
		if jetpack_cooldown == 3.0:
			$JetpackBar.mesh.mid_height = min($JetpackBar.mesh.mid_height + delta, 3.0)
	set_jetpack_particles(jet_particles)

func set_jetpack_particles(value):
	$JetpackParticles.emitting = value
	$JetpackParticles/SpotLight.visible = value

func _process(delta):
	if _get_camera() == null:
		return
		
	update_state()
	update_camera()
	update_jetpack(delta)
	
	if _get_lhand() != null:
		$Robot_Core/Rolling.volume_db = -10
		if ! search.empty():
			move_hands(delta)
		
		if ! modes.empty():
			lift_head(delta)
		
	else:
		roll_head(delta)
