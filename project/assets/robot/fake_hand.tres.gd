extends MeshInstance

export(int) var grabmode = 0
var node = null

func _ready():
	if grabmode == 0:
		node = $"../Robot_Left_Hand/Mesh"
	else:
		node = $"../Robot_Right_Hand/Mesh"

func look_at_cursor():
	global_transform.origin = node.global_transform.origin
	
	var mouse_pos = get_viewport().get_mouse_position()
	var camera    = get_viewport().get_camera()
	if camera == null:
		return
	var world_pos = project_from_cam(mouse_pos, camera)
	
	var look_angle = Vector3(world_pos.x, world_pos.y, 0)
	look_at(look_angle, Vector3.BACK)
	rotate(Vector3.BACK, PI * 0.5)

func do_glowing():
	mesh.surface_get_material(1).albedo_color = Color.white

func no_glowing():
	mesh.surface_get_material(1).albedo_color = Color.black

func _process(delta):
	var robot = get_parent()
	if robot.modes.has(grabmode):
		do_glowing()
	else:
		no_glowing()
		look_at_cursor()
	
func project_from_cam(vec, cam):
	var world_vec = cam.project_position(vec, cam.translation.z)
	return world_vec * Vector3(1, 1, 0)
