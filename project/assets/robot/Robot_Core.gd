extends RigidBody

var last_speed = null

func _process(_data):
	if last_speed != null and \
			last_speed > linear_velocity.length() + 2.0:
		$AudioStreamPlayer.volume_db = -10 + \
			(last_speed - linear_velocity.length()) / 3.0
		$AudioStreamPlayer.play(0.12)
		$AudioStreamPlayer.pitch_scale = rand_range(0.5, 1.5)
		$Eye/AnimationPlayer.play("blink")
	last_speed = linear_velocity.length()
	
	$Rolling.volume_db = -10
	if abs(linear_velocity.x) > 5 and angular_velocity.length() > 1:
		var roll = abs(linear_velocity.x) + angular_velocity.length()
#		print(roll)
		roll = (roll - 6)
		$Rolling.volume_db += min(roll, 30)
